import express from "express";
import json from "body-parser";
import alumnosDb from "../models/alumnos.js";


export const router = express.Router();

export default { router };

// Configurar primer ruta
router.get("/", (req, res) => {
  res.render("index", { titulo: "Practicas de nodeJS" });
});

router.get("/tabla", (req, res) => {
  const params = {
    numero: req.query.numero,
  };
  res.render("partials/practicas/tabla", params);
});

router.post("/tabla", (req, res) => {
  const params = {
    numero: req.body.numero,
  };
  res.render("partials/practicas/tabla", params);
});


//Practica cotizacion

router.get("/cotizacion", (req, res) => {
  const params = {
    valor: req.query.valor,
    pInicial: req.query.pInicial,
    plazos: req.query.plazos,
  };
  res.render("partials/practicas/cotizacion", params);
});

router.post("/cotizacion", (req, res) => {
  const params = {
    valor: req.body.valor,
    pInicial: req.body.pInicial,
    plazos: req.body.plazos,
  };
  res.render("partials/practicas/cotizacion", params);
});


//Practica Alumnos
let rows;

router.get("/alumnos", async(req, res) => {
  rows = await alumnosDb.mostrarTodos();
  res.render("partials/practicas/alumnos",{reg:rows});
});


let params;
router.post('/alumnos', async (req, res) => {
  try {
    params = {
      matricula: req.body.matricula,
      nombre: req.body.nombre,
      domicilio: req.body.domicilio,
      sexo: req.body.sexo,
      especialidad: req.body.especialidad
    }
    const res = await alumnosDb.insertar(params);
  }
  catch(error) {
    console.error(error)
    res.status(400).send("Sucedio un error: " + error);
  }
  rows = await alumnosDb.mostrarTodos();
  res.render("partials/practicas/alumnos",{reg:rows});
});