import conexion from "./conexion.js";
import {resolve} from "path";
import {rejects} from "assert";

var alumnosDb = {}
alumnosDb.insertar = function insertar(alumnos) {
    return new Promise((resolve, rejects) => {
        let sqlconsulta = "Insert into alumnos set ?";
        conexion.query(sqlconsulta, alumnos, function (err, res) {
            if (err) {
                console.log("Surgio un error: " + err);
                rejects(err);
            }
            else {
                const alumno = {
                    id:res.id,

                }
                resolve(alumno);
            }
        })
    });
}

export default alumnosDb;


alumnosDb.mostrarTodos = function mostrarTodos(){
    return new Promise ((resolve,reject)=>{
        let sqlconsulta = "select * from alumnos"
        conexion.query(sqlconsulta,null,function(err,res){
            if(err){
                console.log("Surgio un problema: " + err);
                rejects(err);
            }
            else{
                resolve(res);
            }
        })
    })
};


