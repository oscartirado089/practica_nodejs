// const express = require("express");
import express from 'express';

// const http = require("http");
// import http from 'http';
import { fileURLToPath } from 'url';
import json from 'body-parser';
import path from 'path';

import router from './router/index.js'

const puerto = 80;
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
// Crear la aplicación Express
const main = express();
main.set('view engine', 'ejs');
main.set(express.static( `${__dirname}/public` ) );
main.use(json.urlencoded({extendeds:true}));
main.use(router.router);


main.listen(puerto, () => {
  console.log(`Servidor corriendo en http://localhost:${puerto}`);
});

